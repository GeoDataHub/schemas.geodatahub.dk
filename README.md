# The open geoscientific metadata repository

This repository is a collection of metadata definitions for geoscientific datasets. The site is similar to [schema.org](https://schema.org) with a focus on geoscience. Within the repository, you will find [JSON schema files](http://json-schema.org/) describing the relevant metadata attributes for a few public databases.

**A live version of the repository is available:** [schemas.getgeodata.com](https://schemas.getgeodata.com)

# Using the repository

To learn in the [documentation](https://docs.getgeodata.com/Getting-started/schemas/).

# License

The content of this repository is distributed under a [Creative Commons Share-Alike license](https://creativecommons.org/licenses/by-sa/4.0/).
