from shutil import copytree, rmtree
import os
from glob import glob
import json


def read_schemas():
    """Produce global schema structure

    Read all JSON schema files in all subfolders and produces
    two file. `global_schema` only contains the properties section of
    the schema while `full_global_schema` contains the full schema.
    Each files is formatted as,

      {
        "filename":  { "schema"},
        "filename2": { "schema2"},
      }
    """
    global_schema = {}
    full_global_schema = {}
    for f in glob("schemas/**/*.json", recursive=True):
        print(f"Processing: {f}")
        if 'public' not in f:
            local_schema = json.load(open(f, 'r'))

            # The file path is relative to everything below the schemas folder
            file_path = os.path.relpath(f, 'schemas/')
            path_without_extension = os.path.splitext(file_path)[0].lower()

            if 'properties' in local_schema:
                # The simple case without references to other schemas
                global_schema[path_without_extension] = local_schema['properties']
            elif 'allOf' in local_schema:
                # Schema has parrent schemas
                for child_schema in local_schema['allOf']:
                    if '$ref' not in child_schema:
                        global_schema[path_without_extension] = child_schema

            full_global_schema[path_without_extension] = local_schema
    return global_schema, full_global_schema


if __name__ == "__main__":
    # Remove any existing folders
    rmtree('public', ignore_errors=True)

    # Copy single schemas to public
    copytree("schemas/", "public/")

    # Create super schema
    schemas, full_schemas = read_schemas()
    with open('public/schemas.json', 'w') as f:
        json.dump(schemas, f)
    with open('public/full_schemas.json', 'w') as f:
        json.dump(full_schemas, f)
