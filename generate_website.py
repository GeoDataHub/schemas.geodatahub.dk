#!/usr/bin/env python
import json
from jinja2 import Environment, FileSystemLoader

file_loader = FileSystemLoader('templates')
env = Environment(loader=file_loader)
test_template = env.get_template('index.html')
schemas = json.load(open('public/full_schemas.json', 'r'))

site = {
    "name": "GetGeoData",
    "tagline": "Schema Repository",
    "url": "https://schemas.getgeodata.com"
}

with open('public/index.html', 'w') as fout:
    fout.write(test_template.render(schemas=schemas, site=site))

# Generate a schema page for each schema
schema_template = env.get_template('schema.html')
for schema_path, schema in schemas.items():
    with open(f'public/{schema_path}.html', 'w') as fout:
        schema_basename = schema_path.split('/')[-1]
        fout.write(schema_template.render(schema=schema, schema_path=schema_path, schema_name=schema_basename, site=site))
