setup:
	pip3 install -r requirements.txt

build:
	python3 create_super_schema.py
	python3 generate_website.py

deploy:
	# Copy files to AWS
	aws s3 cp public/ s3://schemas.geodatahub.dk/ --recursive
